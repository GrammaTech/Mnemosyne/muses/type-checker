from pathlib import Path
import subprocess
import os

def run_incremental_type(src_directory: Path):
    cmd = ["pyre", "infer"]
    proc = subprocess.call(cmd, cwd=src_directory)

def parse_incremental_types(type_dir: Path, type_map):
    file_paths = type_dir.glob('**/*.pyi')
    for type_file in file_paths:
        f = open(type_file, "r")
        f_map = {}
        for line in f:
            sp_line = line.split()
            f_name = sp_line[1]
            t_name = sp_line[3][:-1]
            f_map[f_name]=t_name
        type_map[type_file.name[:-1]] = f_map

def main():
    path = Path('../test').absolute()
    run_incremental_type(path)
    inferred_location = path / '.pyre' / 'types'
    type_map = {}
    parse_incremental_types(inferred_location, type_map)
    print(type_map)

if __name__ == "__main__":
    main()

